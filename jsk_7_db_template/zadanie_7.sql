-- -- -------------------------------------------------------------------------------
-- -- Zadanie 7
-- -- -------------------------------------------------------------------------------

-- -- -------------------------------------------------------------------------------
-- Section: setting sql_mode
-- -- -------------------------------------------------------------------------------

SET sql_mode='ONLY_FULL_GROUP_BY,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -- -------------------------------------------------------------------------------
-- Section: USE
-- Zamien 'jsk_v7_jan_kowalski' na poprawna nazwe Twojej bazy danych
-- -- -------------------------------------------------------------------------------
USE jsk_v7_lukasz_michalczak;

-- -- -------------------------------------------------------------------------------
-- Section: rozwiazanie zadania
-- -- -------------------------------------------------------------------------------
-- DROP
DROP USER IF EXISTS 'user_ro';
DROP USER IF EXISTS 'user_rw_adres';
DROP USER IF EXISTS 'user_admin';

-- CREATE
CREATE USER 'user_ro'
  IDENTIFIED BY 'ro';
  
CREATE USER 'user_rw_adres'
	IDENTIFIED BY 'rw_adres';
    
CREATE USER 'user_admin'
	IDENTIFIED BY 'admin';
    
-- a)	użytkownik user_ro może tylko odczytywać dane z wszystkich tabel
GRANT SELECT 
ON jsk_v7_lukasz_michalczak.* 
TO 'user_ro';

-- b)	użytkownik user_rw_adres ma pełny dostęp do tabeli z adresami (tzn. może wstawiać i usuwać dane), do innych tabel w projekcie ma tylko prawo odczytu
GRANT ALL PRIVILEGES 
ON jsk_v7_lukasz_michalczak.DaneKontaktowe 
TO 'user_rw_adres';

-- c)	użytkownik user_admin ma pełny dostęp do wszystkich uprawnień
GRANT ALL PRIVILEGES 
ON *.*
TO 'user_admin'
WITH GRANT OPTION;