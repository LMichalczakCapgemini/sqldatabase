-- -- -------------------------------------------------------------------------------
-- -- Zadanie 4
-- -- -------------------------------------------------------------------------------

-- -- -------------------------------------------------------------------------------
-- Section: setting sql_mode
-- -- -------------------------------------------------------------------------------

SET sql_mode='ONLY_FULL_GROUP_BY,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -- -------------------------------------------------------------------------------
-- Section: USE
-- -- -------------------------------------------------------------------------------
USE jsk_v7_lukasz_michalczak;

-- -- -------------------------------------------------------------------------------
-- Section: rozwiazanie zadania
-- -- -------------------------------------------------------------------------------


-- 1)	wylistuj wszystkie tabele w Twojej bazie danych
SELECT TABLE_NAME
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = 'jsk_v7_lukasz_michalczak';

-- 2)	wyświetl informacje na temat pól i typów danych w tabeli pracowników
SELECT COLUMN_NAME, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH, 
       NUMERIC_PRECISION, DATETIME_PRECISION, 
       IS_NULLABLE 
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME='Pracownicy';

-- 3)	znajdź pracowników starszych niż 25 lat
-- Pracownicy posortowni wiekiem
SELECT 
    Imię, Nazwisko, DataUrodzenia
FROM
    Pracownicy JOIN
    DaneOsobowe ON Pracownicy.FK_DaneOsobowe_id = DaneOsobowe.Id
ORDER BY DataUrodzenia ASC;

SELECT 
    Imię, Nazwisko, DataUrodzenia
FROM
    Pracownicy JOIN
    DaneOsobowe ON Pracownicy.FK_DaneOsobowe_id = DaneOsobowe.Id
WHERE
    TIMESTAMPDIFF(year, DataUrodzenia, CURDATE()) > 25
    OR TIMESTAMPDIFF(year, DataUrodzenia, CURDATE()) = 25 AND month(CURDATE()) > month(DataUrodzenia)
    OR TIMESTAMPDIFF(year, DataUrodzenia, CURDATE()) = 25 AND month(CURDATE()) = month(DataUrodzenia) AND day(CURDATE()) > day(DataUrodzenia);

-- 4)	znajdź pracowników, których nazwisko jest dłuższe niż 5 znaków, uwzględnij przy tym polskie znaki czy znaki specjalne
-- nazwiska posortowane względem długości
SELECT Nazwisko, CHAR_LENGTH(Nazwisko) AS Długość
FROM Pracownicy 
	JOIN DaneOsobowe 
		ON Pracownicy.FK_DaneOsobowe_Id = DaneOsobowe.Id
ORDER BY Długość ASC;

SELECT Imię, Nazwisko
FROM Pracownicy
        JOIN DaneOsobowe 
			ON Pracownicy.FK_DaneOsobowe_Id = DaneOsobowe.Id
WHERE CHAR_LENGTH(Nazwisko) > 5;

-- 5)	znajdź pracowników, w których nazwisku na drugim miejscu wystepuje litera 'a'
-- posortowane po drugiej literze
SELECT Nazwisko
FROM Pracownicy
    JOIN DaneOsobowe 
		ON Pracownicy.FK_DaneOsobowe_Id = DaneOsobowe.Id
ORDER BY SUBSTRING(Nazwisko, 2, 1) ASC;

SELECT Imię, Nazwisko
FROM Pracownicy
	JOIN DaneOsobowe 
    ON Pracownicy.FK_DaneOsobowe_Id = DaneOsobowe.Id
WHERE Nazwisko LIKE '_a%'; -- TODO ~'ą'

-- 6)	znajdź klientów, którzy posiadają e-mail, w którym na drugim miejscu local-part (znaki przed "@") nie znajduje się litera, a domeną jest ".pl" lub ".eu"
SELECT Imię, Nazwisko,
    Email, SUBSTRING(SUBSTRING_INDEX(Email, '@', 1), 2, 1) AS DrugaLitera,
    SUBSTRING_INDEX(Email, '@', - 1) AS Domena
FROM Klienci 
	JOIN DaneKontaktowe 
		ON Klienci.FK_DaneKontaktowe_Id = DaneKontaktowe.Id
	JOIN DaneOsobowe 
		ON Klienci.FK_DaneOsobowe_Id = DaneOsobowe.Id
ORDER BY Domena DESC;

SELECT Imię, Nazwisko
FROM Klienci
	JOIN DaneKontaktowe 
		ON Klienci.FK_DaneKontaktowe_Id = DaneKontaktowe.Id
	JOIN DaneOsobowe 
		ON Klienci.FK_DaneOsobowe_Id = DaneOsobowe.Id 
WHERE SUBSTRING(SUBSTRING_INDEX(Email, '@', 1),2,1) REGEXP '[^A-z]'
	AND (SUBSTRING_INDEX(Email, '@', - 1) = '.pl' OR SUBSTRING_INDEX(Email, '@', - 1) = '.eu');
    
-- 7)	znajdź klientów, którzy posiadają e-mail niezgodny z przyjętym standardem formatu adresu e-mail (np. test@wp. zamiast test@wp.pl)
SELECT Imię, Nazwisko,
    Email,SUBSTRING_INDEX(Email, '@', 1) AS LocalPart, SUBSTRING_INDEX(Email, '@', - 1) AS Domena
FROM Klienci 
	JOIN DaneKontaktowe 
		ON Klienci.FK_DaneKontaktowe_Id = DaneKontaktowe.Id
	JOIN DaneOsobowe 
		ON Klienci.FK_DaneOsobowe_Id = DaneOsobowe.Id
ORDER BY Domena DESC;
	
SELECT Imię, Nazwisko
FROM Klienci
	JOIN DaneKontaktowe 
		ON Klienci.FK_DaneKontaktowe_Id = DaneKontaktowe.Id
	JOIN DaneOsobowe 
		ON Klienci.FK_DaneOsobowe_Id = DaneOsobowe.Id
WHERE Email NOT REGEXP '^[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$';

-- 8)	policz ilość samochodów których przebieg jest pomiędzy 200 tys. km a 300 tys. km
-- samochody posortowane przebiegiem
SELECT Id, Przebieg_km
FROM Samochody
ORDER BY Przebieg_km ASC;

SELECT count(Id)
FROM Samochody
WHERE Przebieg_km BETWEEN 200000 AND 300000;

-- 9)	znajdź wszystkich kierowników
-- Pracownicy posortowani po stanowisku
SELECT Imię, Nazwisko, Stanowisko
FROM Pracownicy
	JOIN DaneOsobowe
		ON Pracownicy.FK_DaneOsobowe_Id = DaneOsobowe.Id
ORDER BY Stanowisko ASC;

SELECT Imię, Nazwisko
FROM Pracownicy
	JOIN DaneOsobowe
		ON Pracownicy.FK_DaneOsobowe_Id = DaneOsobowe.Id
WHERE Stanowisko = 'KIEROWNIK';

-- 10)	znajdź samochody z największym przebiegiem
-- samochody z posortowanym przebiegiem
SELECT Id, Przebieg_km
FROM Samochody
ORDER BY Przebieg_km DESC;

SELECT Id
FROM Samochody
WHERE Przebieg_km = (SELECT MAX(Przebieg_km)
        FROM  Samochody);

-- 11)	znajdź klienta z największą liczbą wypożyczeń 
-- klienci posortowani po iloiści zamówień
SELECT Imię, Nazwisko, Count(FK_Klient_Id) AS IlośćZamówień
FROM Wypożyczenia
    JOIN Klienci 
		ON Wypożyczenia.FK_Klient_Id = Klienci.Id
	JOIN DaneOsobowe
		ON Klienci.FK_DaneOsobowe_Id = DaneOsobowe.Id
GROUP BY FK_Klient_Id
ORDER BY IlośćZamówień DESC;

-- Z having
SELECT Imię, Nazwisko
FROM Wypożyczenia
    JOIN Klienci 
		ON Wypożyczenia.FK_Klient_Id = Klienci.Id
	JOIN DaneOsobowe
		ON Klienci.FK_DaneOsobowe_Id = DaneOsobowe.Id
GROUP BY FK_Klient_Id
HAVING Count(FK_Klient_Id) = (SELECT MAX(IlośćWypożyczeń) FROM 
(SELECT Count(FK_Klient_Id) AS IlośćWypożyczeń
FROM Wypożyczenia
    JOIN Klienci 
		ON Wypożyczenia.FK_Klient_Id = Klienci.Id
	JOIN DaneOsobowe
		ON Klienci.FK_DaneOsobowe_Id = DaneOsobowe.Id
GROUP BY FK_Klient_Id) TablicaIliściWypożyczeń);

-- Bez having
SELECT Imię, Nazwisko
FROM (SELECT Imię, Nazwisko, Count(FK_Klient_Id) AS IlośćWypożyczeń
	FROM Wypożyczenia
		JOIN Klienci 
			ON Wypożyczenia.FK_Klient_Id = Klienci.Id
		JOIN DaneOsobowe
			ON Klienci.FK_DaneOsobowe_Id = DaneOsobowe.Id
	GROUP BY FK_Klient_Id) KlienciZIlościamiWypożyczeń
WHERE KlienciZIlościamiWypożyczeń.IlośćWypożyczeń = (SELECT MAX(IlośćWypożyczeń) 
	FROM (SELECT Imię, Nazwisko, Count(FK_Klient_Id) AS IlośćWypożyczeń
	FROM Wypożyczenia
		JOIN Klienci 
			ON Wypożyczenia.FK_Klient_Id = Klienci.Id
		JOIN DaneOsobowe
			ON Klienci.FK_DaneOsobowe_Id = DaneOsobowe.Id
	GROUP BY FK_Klient_Id) MaxIlośćWypożyczeń);

-- 12)	podaj liczbę wszystkich wypożyczeń w każdym miesiącu, niezależnie od roku. Uwzględnij tylko te, w których były jakieś wypożyczenia
SELECT DataWypożyczenia, MONTH(DataWypożyczenia) AS MiesiącWypożyczenia
FROM Wypożyczenia
ORDER BY MiesiącWypożyczenia;

SELECT MONTH(DataWypożyczenia) AS MiesiącWypożyczenia, COUNT(Id) IlośćWypożyczeńWMiesiącu
FROM Wypożyczenia
GROUP BY MiesiącWypożyczenia;

-- 13)	dla każdego miesiąca podaj średnią liczbę wypożyczeń na klienta
SELECT MONTH(DataWypożyczenia) AS MiesiącWypożyczenia, COUNT(Id) IlośćWypożyczeńWMiesiącu
FROM Wypożyczenia
GROUP BY MiesiącWypożyczenia;

SELECT COUNT(Id) FROM Klienci;

SELECT MONTH(DataWypożyczenia) AS MiesiącWypożyczenia, COUNT(Id)/(SELECT COUNT(Id) FROM Klienci) ŚredniaIlośćWypożyczeńNaKlienta
FROM Wypożyczenia
GROUP BY MiesiącWypożyczenia;

-- 14)	znajdź klientów, którzy wypożyczyli największą liczbę różnych samochodów
SELECT Imię, Nazwisko, FK_Samochód_Id
FROM Wypożyczenia
    JOIN Klienci 
		ON Wypożyczenia.FK_Klient_Id = Klienci.Id
	JOIN DaneOsobowe
		ON Klienci.FK_DaneOsobowe_Id = DaneOsobowe.Id
ORDER BY FK_Klient_Id;

-- z HAVING
SELECT Imię, Nazwisko
FROM Wypożyczenia
    JOIN Klienci 
		ON Wypożyczenia.FK_Klient_Id = Klienci.Id
	JOIN DaneOsobowe
		ON Klienci.FK_DaneOsobowe_Id = DaneOsobowe.Id
GROUP BY FK_Klient_Id
HAVING COUNT(DISTINCT FK_Samochód_Id) = (SELECT MAX(IlośćUnikatowychZamówień) FROM (
SELECT COUNT(DISTINCT FK_Samochód_Id) AS IlośćUnikatowychZamówień
FROM Wypożyczenia
    JOIN Klienci 
		ON Wypożyczenia.FK_Klient_Id = Klienci.Id
GROUP BY FK_Klient_Id) MaksymalnaIlośćZamówieńNaKlienta);

-- Bez HAVING
SELECT Imię, Nazwisko
FROM (SELECT Imię, Nazwisko, COUNT(DISTINCT FK_Samochód_Id) AS IlośćRóżnychWypożyczeń
	FROM Wypożyczenia
    JOIN Klienci 
		ON Wypożyczenia.FK_Klient_Id = Klienci.Id
	JOIN DaneOsobowe
		ON Klienci.FK_DaneOsobowe_Id = DaneOsobowe.Id
GROUP BY FK_Klient_Id) IlościWypożyczeńKLientów
WHERE IlościWypożyczeńKLientów.IlośćRóżnychWypożyczeń = (SELECT MAX(IlośćRóżnychWypożyczeń)
	FROM( SELECT COUNT(DISTINCT FK_Samochód_Id) AS IlośćRóżnychWypożyczeń
	FROM Wypożyczenia
    JOIN Klienci 
		ON Wypożyczenia.FK_Klient_Id = Klienci.Id
	GROUP BY FK_Klient_Id) MaxIlośćRóżnychWypożyczeń);

-- 15)	znajdź marki samochodów najczęściej wypożyczanych przez klientów
-- posortowane wypożyczenia danej marki
SELECT Marka, Count(FK_Samochód_Id) AS IlośćWypożyczeń
FROM Wypożyczenia
	JOIN Samochody
		ON Wypożyczenia.FK_Samochód_Id = Samochody.Id
GROUP BY Marka
ORDER BY IlośćWypożyczeń DESC;

SELECT Marka
FROM (SELECT Marka, COUNT(FK_Samochód_Id) AS IlośćWypożyczeń
    FROM Wypożyczenia
    JOIN Samochody 
		ON Wypożyczenia.FK_Samochód_Id = Samochody.Id
    GROUP BY Marka) TabelaZIlościamiWypożyczeń
WHERE TabelaZIlościamiWypożyczeń.IlośćWypożyczeń = (SELECT MAX(IlośćWypożyczeń) AS MaxIlośćWypożyczeń
        FROM (SELECT COUNT(FK_Samochód_Id) AS IlośćWypożyczeń
            FROM Wypożyczenia
            JOIN Samochody 
				ON Wypożyczenia.FK_Samochód_Id = Samochody.Id
            GROUP BY Marka
            ORDER BY IlośćWypożyczeń DESC) MaksymalnaIloscWyopżyczeń);
        
-- 16)	znajdź klientów, którzy zapłacili najwięcej za wypożyczenia w bieżącym roku. Wykorzystaj widok
SELECT Imię, Nazwisko, Koszt
FROM Wypożyczenia
	JOIN Klienci
		ON Wypożyczenia.FK_Klient_Id = Klienci.Id
	JOIN DaneOsobowe
		ON Klienci.FK_DaneOsobowe_ID = DaneOsobowe.Id
WHERE YEAR(DataWypożyczenia) = YEAR(NOW())
ORDER BY Nazwisko;

-- Widok Zwracający klientów z kosztami wypożyczeń w bieżącym roku
CREATE OR REPLACE VIEW v_ZamówieniaKlientaWDanymRoku AS
SELECT FK_Klient_Id, Imię, Nazwisko, Koszt
FROM Wypożyczenia
	JOIN Klienci
		ON Wypożyczenia.FK_Klient_Id = Klienci.Id
	JOIN DaneOsobowe
		ON Klienci.FK_DaneOsobowe_ID = DaneOsobowe.Id
WHERE YEAR(DataWypożyczenia) = YEAR(NOW());

SELECT Imię, Nazwisko
FROM ( SELECT Imię, Nazwisko, SUM(Koszt) AS SumaKosztówKlientaWRoku
	FROM v_ZamówieniaKlientaWDanymRoku
	GROUP BY FK_Klient_Id
	) AS TabelaSumyKosztówklientówWRoku
WHERE TabelaSumyKosztówklientówWRoku.SumaKosztówKlientaWRoku = (SELECT MAX(KosztZamówień) 
	FROM (SELECT SUM(Koszt) as KosztZamówień
		FROM v_ZamówieniaKlientaWDanymRoku
		GROUP BY FK_Klient_Id) MaksymalnyKosztZamówień);

-- 17)	znajdź klientów, którzy wypożyczyli samochód marki X w dacie między Y a Z
-- Klienci z datą wypożyczenia i marką
SELECT Imię, Nazwisko, DataWypożyczenia, DataOddania, Marka
FROM Wypożyczenia
	JOIN Klienci
		ON Wypożyczenia.FK_Klient_Id = Klienci.Id
	JOIN DaneOsobowe
		ON Klienci.FK_DaneOsobowe_ID = DaneOsobowe.Id
	JOIN Samochody
		ON Wypożyczenia.FK_Samochód_id = Samochody.Id
ORDER BY DataWypożyczenia, Marka;

SELECT Imię, Nazwisko
FROM Wypożyczenia
	JOIN Klienci
		ON Wypożyczenia.FK_Klient_Id = Klienci.Id
	JOIN DaneOsobowe
		ON Klienci.FK_DaneOsobowe_ID = DaneOsobowe.Id
	JOIN Samochody
		ON Wypożyczenia.FK_Samochód_id = Samochody.Id
WHERE Marka = 'OPEL'
	AND DataWypożyczenia > '2017-07-20' 
    AND DataOddania < '2018-08-01'
GROUP BY FK_Klient_Id;

-- 18)	znajdź klientów, którzy mieli choć przez chwilę wypożyczony samochód marki X w dacie między Y a Z. Weź pod uwagę różne możliwości nakładania się terminów
-- Klienci z datą wypożyczenia i marką
SELECT Imię, Nazwisko, DataWypożyczenia, DataOddania, Marka
FROM Wypożyczenia
	JOIN Klienci
		ON Wypożyczenia.FK_Klient_Id = Klienci.Id
	JOIN DaneOsobowe
		ON Klienci.FK_DaneOsobowe_ID = DaneOsobowe.Id
	JOIN Samochody
		ON Wypożyczenia.FK_Samochód_id = Samochody.Id
ORDER BY DataWypożyczenia, Marka;

SELECT Imię, Nazwisko
FROM Wypożyczenia
	JOIN Klienci
		ON Wypożyczenia.FK_Klient_Id = Klienci.Id
	JOIN DaneOsobowe
		ON Klienci.FK_DaneOsobowe_ID = DaneOsobowe.Id
	JOIN Samochody
		ON Wypożyczenia.FK_Samochód_id = Samochody.Id
WHERE Marka = 'OPEL'
	AND DataOddania > '2017-10-23' 
    AND DataWypożyczenia < '2018-08-01'
GROUP BY FK_Klient_Id;

-- 19)	znajdź klienta, który najczęściej wypożycza i oddaje samochody w innych miejscach
-- Klienci z miejscem wypożyczenia i oddania
SELECT Imię, Nazwisko, FK_PlacówkaOddania_Id AS PlacówkaOddania, FK_PlacówkaWypożyczenia_Id AS PlacówkaWypożyczenia
FROM Wypożyczenia
	JOIN Klienci
		ON Wypożyczenia.FK_Klient_Id = Klienci.Id
	JOIN DaneOsobowe
		ON Klienci.FK_DaneOsobowe_ID = DaneOsobowe.Id
ORDER BY Nazwisko;


SELECT  Imię, Nazwisko
FROM (SELECT Imię, Nazwisko, Count(FK_Klient_Id) as IlośćOddaniaWInnymMiejscu
	FROM Wypożyczenia
		JOIN Klienci
			ON Wypożyczenia.FK_Klient_Id = Klienci.Id
		JOIN DaneOsobowe
			ON Klienci.FK_DaneOsobowe_ID = DaneOsobowe.Id
	WHERE FK_PlacówkaOddania_Id <> FK_PlacówkaWypożyczenia_Id
	GROUP BY FK_Klient_Id) TabelaIlosciOddańKlientówWInnychMiejscach
WHERE  IlośćOddaniaWInnymMiejscu = (SELECT MAX(IlośćOddaniaWInnymMiejscu) FROM
	(SELECT Count(FK_Klient_Id) as IlośćOddaniaWInnymMiejscu
	FROM Wypożyczenia
		JOIN Klienci
			ON Wypożyczenia.FK_Klient_Id = Klienci.Id
		JOIN DaneOsobowe
			ON Klienci.FK_DaneOsobowe_ID = DaneOsobowe.Id
	WHERE FK_PlacówkaOddania_Id <> FK_PlacówkaWypożyczenia_Id
	GROUP BY FK_Klient_Id)  MaxOddańWInnymMiejscu);

-- 20)	znajdź domenę, w której klienci najczęściej posiadają adres e-mail (np. gmail.com albo wp.pl)
SELECT SUBSTR(email, INSTR(email, '@') + 1) AS Domena
FROM Klienci 
	JOIN DaneKontaktowe 
		ON Klienci.FK_DaneKontaktowe_Id = DaneKontaktowe.Id;
        
SELECT Domena
FROM ( SELECT SUBSTR(email, INSTR(email, '@') + 1) AS Domena, COUNT(*) AS IolśćWystąpieńDomeny
	FROM Klienci 
		JOIN DaneKontaktowe 
			ON Klienci.FK_DaneKontaktowe_Id = DaneKontaktowe.Id
	GROUP BY Domena) IlościWystąpieńDomen
WHERE IlościWystąpieńDomen.IolśćWystąpieńDomeny = (SELECT MAX(IlośćWystąpieńDomeny) AS MaksymalnaIlośćWystąpieńDomeny
FROM (
	SELECT COUNT(1) AS IlośćWystąpieńDomeny, SUBSTR(email, INSTR(email, '@') + 1) AS Domena
    FROM Klienci 
		JOIN DaneKontaktowe 
			ON Klienci.FK_DaneKontaktowe_Id = DaneKontaktowe.Id
	GROUP BY Domena 
    ) AS MaxIlośćWystąpieńDomeny);
    
-- 21)	znajdź samochody, które mają co najmniej 2 opiekunów
-- Samochody i opiekunwie posortowani po opoekunach
SELECT *
FROM Pracownik2Samochód
ORDER BY Samochód_Id ASC;
        
SELECT Samochód_Id AS Samochód
FROM Pracownik2Samochód
GROUP BY Samochód 
HAVING COUNT(Samochód) >= 2;

-- 22)	znajdź pracowników, którzy nie opiekują się żadnym samochodem
-- Pracownicy opiekujący się jakims samochodem
SELECT Imię, Nazwisko
FROM Pracownik2Samochód
	JOIN Pracownicy
		ON Pracownik_Id = Pracownicy.Id
	JOIN DaneOsobowe
		ON Pracownicy.FK_DaneOsobowe_Id = DaneOsobowe.Id
GROUP BY Pracownik_Id
ORDER BY Nazwisko;

SELECT Imię, Nazwisko
FROM Pracownicy
	JOIN DaneOsobowe
		ON Pracownicy.FK_DaneOsobowe_Id = DaneOsobowe.Id
WHERE Pracownicy.Id NOT IN (SELECT Pracownik_Id
	FROM Pracownik2Samochód);

-- 23)	zaktualizuj kolor samochodu X
SELECT KOLOR FROM SAMOCHODY WHERE Id = 1;

UPDATE Samochody 
SET Kolor ='biały'
WHERE Id = 1;

SELECT KOLOR FROM SAMOCHODY WHERE Id = 1;