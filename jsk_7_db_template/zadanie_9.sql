-- -- -------------------------------------------------------------------------------
-- -- Zadanie 9
-- -- -------------------------------------------------------------------------------

-- -- -------------------------------------------------------------------------------
-- Section: setting sql_mode
-- -- -------------------------------------------------------------------------------

SET sql_mode='ONLY_FULL_GROUP_BY,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -- -------------------------------------------------------------------------------
-- Section: USE
-- Zamien 'jsk_v7_jan_kowalski' na poprawna nazwe Twojej bazy danych
-- -- -------------------------------------------------------------------------------
USE jsk_v7_lukasz_michalczak;

-- -- -------------------------------------------------------------------------------
-- Section: rozwiazanie zadania
-- -- -------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS mojaProcedura;
CREATE PROCEDURE mojaProcedura()
BEGIN
	DROP TABLE IF EXISTS WynikProcedury;
	CREATE TABLE WynikProcedury (
    id INT NOT NULL AUTO_INCREMENT,
    rokMiesiąc VARCHAR(6),
    obrót DECIMAL(6,2),
    liczbaWypożyczeń INT,
    liczbaUnikalnychKlientów INT,
    liczbaPlacówekZWypożyczeniami INT,
    PRIMARY KEY(id)
    );

INSERT INTO WynikProcedury (rokMiesiąc,obrót,liczbaWypożyczeń,liczbaUnikalnychKlientów,liczbaPlacówekZWypożyczeniami)
	SELECT EXTRACT(YEAR_MONTH FROM DataWypożyczenia) AS RokMiesiąc, SUM(Koszt) AS SumaWRokMiesiąc,
		COUNT(Id) AS Wypożyczenia, COUNT(DISTINCT FK_Klient_Id) AS UnikalniKlienci, 
        COUNT(DISTINCT FK_PlacówkaWypożyczenia_Id) AS PlacówkiZwypożyczeniami
	FROM Wypożyczenia
	GROUP BY RokMiesiąc;
END //
DELIMITER ;

call mojaProcedura();

-- Sprawdzenie
SELECT *
FROM WynikProcedury 
ORDER BY rokMiesiąc;
        
SELECT *
FROM Wypożyczenia 
ORDER BY DataWypożyczenia;
-- SELECT * FROM WynikProcedury;
