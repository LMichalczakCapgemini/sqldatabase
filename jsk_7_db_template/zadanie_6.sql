-- -- -------------------------------------------------------------------------------
-- -- Zadanie 6
-- -- -------------------------------------------------------------------------------

-- -- -------------------------------------------------------------------------------
-- Section: setting sql_mode
-- -- -------------------------------------------------------------------------------

SET sql_mode='ONLY_FULL_GROUP_BY,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -- -------------------------------------------------------------------------------
-- Section: USE
-- -- -------------------------------------------------------------------------------
USE jsk_v7_lukasz_michalczak;

-- -- -------------------------------------------------------------------------------
-- Section: rozwiazanie zadania
-- -- -------------------------------------------------------------------------------

DROP TRIGGER IF EXISTS minKosztWypożyczenia
DELIMITER //
CREATE TRIGGER minKosztWypożyczenia BEFORE INSERT ON Wypożyczenia
    FOR EACH ROW BEGIN 
    IF(NEW.Koszt < 100) THEN
		SET NEW.Koszt = 100;
	END IF;
	END//
DELIMITER ;

SELECT Id, Koszt 
FROM Wypożyczenia
ORDER BY Id DESC;

INSERT INTO Wypożyczenia (DataWypożyczenia, DataOddania, koszt, FK_PlacówkaOddania_Id, FK_PlacówkaWypożyczenia_Id, FK_Samochód_Id, FK_Klient_Id)
VALUES (CURRENT_DATE(),CURRENT_DATE(),10.99,1,1,1,5);

INSERT INTO Wypożyczenia (DataWypożyczenia, DataOddania, koszt, FK_PlacówkaOddania_Id, FK_PlacówkaWypożyczenia_Id, FK_Samochód_Id, FK_Klient_Id)
VALUES (CURRENT_DATE(),CURRENT_DATE(),100.01,1,1,1,5);