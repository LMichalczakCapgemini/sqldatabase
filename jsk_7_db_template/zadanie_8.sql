-- -- -------------------------------------------------------------------------------
-- -- Zadanie 8
-- -- -------------------------------------------------------------------------------

-- -- -------------------------------------------------------------------------------
-- Section: setting sql_mode
-- -- -------------------------------------------------------------------------------

SET sql_mode='ONLY_FULL_GROUP_BY,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -- -------------------------------------------------------------------------------
-- Section: USE
-- Zamien 'jsk_v7_jan_kowalski' na poprawna nazwe Twojej bazy danych
-- -- -------------------------------------------------------------------------------
USE jsk_v7_lukasz_michalczak;

-- -- -------------------------------------------------------------------------------
-- Section: rozwiazanie zadania
-- -- -------------------------------------------------------------------------------
-- procedura usuwająca index jeśli istnieje - zaczerpnięta ze strony 
-- https://stackoverflow.com/questions/2480148/how-can-i-employ-if-exists-for-creating-or-dropping-an-index-in-mysql
DELIMITER $$
DROP PROCEDURE IF EXISTS drop_index_if_exists $$
CREATE PROCEDURE drop_index_if_exists(in theTable varchar(128), in theIndexName varchar(128) )
BEGIN
 IF((SELECT COUNT(*) AS index_exists FROM information_schema.statistics WHERE TABLE_SCHEMA = DATABASE() and table_name =
theTable AND index_name = theIndexName) > 0) THEN
   SET @s = CONCAT('DROP INDEX ' , theIndexName , ' ON ' , theTable);
   PREPARE stmt FROM @s;
   EXECUTE stmt;
 END IF;
END $$
DELIMITER ;

call drop_index_if_exists('DaneOsobowe','StałaDataUrodzenia');
call drop_index_if_exists('Samochody','StałeDaneSamochodu');

-- a)	załóż 2 indeksy na wybranych przez siebie kolumnach
CREATE INDEX StałaDataUrodzenia
ON DaneOsobowe (DataUrodzenia);

CREATE INDEX StałeDaneSamochodu
ON Samochody (TypSamochodu, Marka, RokProdukcji, Kolor, PojemnośćSilnika, MocSilnika_KM);

-- b)	uzasadnij dlaczego tam powinny się znaleźć (odpowiedz komentarzem poniżej)
/*
Środowsko udziela informacji, że w poniższych zapytaniach użyte zostają stworzone indeksy:
EXPLAIN SELECT DataUrodzenia
FROM DaneOsobowe;

EXPLAIN SELECT TypSamochodu, Marka, RokProdukcji, Kolor, PojemnośćSilnika, MocSilnika_KM
FROM Samochody

ad.a
Data urodzenia nie ulega zmianie
ad.b
Samochody nie sa czesto dodawane, ani usuwane. Kolumny (właściwości samochodu) na które nałożony jest index są stałe

*/
-- c)	jakie są pozytywne a jakie negatywne skutki zastosowania indeksów?
/*
Korzystając z odpowiednich indeksów można w znaczący sposób skrócić czas wykonywania zapytania do bazy danych. 
Kosztem indeksu jest miejsce na dysku oraz czas związany z operacjami modyfikacji danych (INSERT, UPDATE, DELETE).
 */