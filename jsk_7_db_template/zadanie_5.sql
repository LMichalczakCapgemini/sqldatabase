-- -- -------------------------------------------------------------------------------
-- -- Zadanie 5
-- -- -------------------------------------------------------------------------------

-- -- -------------------------------------------------------------------------------
-- Section: setting sql_mode
-- -- -------------------------------------------------------------------------------

SET sql_mode='ONLY_FULL_GROUP_BY,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -- -------------------------------------------------------------------------------
-- Section: USE
-- -- -------------------------------------------------------------------------------
USE jsk_v7_lukasz_michalczak;

-- -- -------------------------------------------------------------------------------
-- Section: rozwiazanie zadania
-- -- -------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS RokMiesiacNajwiecejWypozyczen;
DELIMITER //
CREATE FUNCTION RokMiesiacNajwiecejWypozyczen(klientId INT)
RETURNS VARCHAR(100) DETERMINISTIC
BEGIN

	SELECT GROUP_CONCAT(RokMiesiacWypożyczenia) INTO @RokMIesiac
	FROM (SELECT EXTRACT(YEAR_MONTH FROM DataWypożyczenia) AS RokMiesiacWypożyczenia, Count(*) AS IlośćWypożyczeń
		FROM Wypożyczenia
		WHERE FK_Klient_Id = klientId
		GROUP BY RokMiesiacWypożyczenia
		) TabelaRokMiesiąc
	WHERE TabelaRokMiesiąc.IlośćWypożyczeń = (SELECT MAX(IlośćWypożyczeń)
	FROM (SELECT EXTRACT(YEAR_MONTH FROM DataWypożyczenia) AS RokMiesiacWypożyczenia, Count(*) AS IlośćWypożyczeń
		FROM Wypożyczenia
		WHERE FK_Klient_Id = klientId
		GROUP BY RokMiesiacWypożyczenia	) MaksymalnaIolśćWypożyczeń);
	
    RETURN @RokMIesiac;
END//
DELIMITER ;

-- posortowane wypożyczenia klientów
SELECT Imię, Nazwisko, DataWypożyczenia
FROM Wypożyczenia
	JOIN Klienci
		ON Wypożyczenia.FK_Klient_Id = Klienci.Id
    JOIN DaneOsobowe
		ON Wypożyczenia.FK_Klient_Id = DaneOsobowe.Id
Order By FK_Klient_Id, DataWypożyczenia;

SELECT Imię, Nazwisko, RokMiesiacNajwiecejWypozyczen(Klienci.Id) AS RokMiesiacNajczęstrzychWypożyczeń	
FROM Wypożyczenia
	JOIN Klienci
		ON Wypożyczenia.FK_Klient_Id = Klienci.Id
    JOIN DaneOsobowe
		ON Wypożyczenia.FK_Klient_Id = DaneOsobowe.Id
GROUP BY FK_Klient_Id;