-- -- -------------------------------------------------------------------------------
-- -- Zadanie 3
-- -- -------------------------------------------------------------------------------

-- -- -------------------------------------------------------------------------------
-- Section: setting sql_mode
-- -- -------------------------------------------------------------------------------

SET sql_mode='ONLY_FULL_GROUP_BY,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -- -------------------------------------------------------------------------------
-- Section: USE
-- -- -------------------------------------------------------------------------------
USE jsk_v7_lukasz_michalczak;

-- -- -------------------------------------------------------------------------------
-- Section: rozwiazanie zadania
-- -- -------------------------------------------------------------------------------

CREATE OR REPLACE VIEW v_ImieNazwiskoKlienta_DatyWypożyczenia_Marka AS
SELECT Imię, Nazwisko, DataWypożyczenia, DataOddania, Marka
FROM Wypożyczenia
	JOIN Klienci 
		ON Klienci.Id = Wypożyczenia.FK_Klient_Id
	JOIN DaneOsobowe
		On DaneOsobowe.id = Klienci.FK_DaneOsobowe_Id
	JOIN Samochody
		On Wypożyczenia.FK_Samochód_Id = Samochody.Id
	
