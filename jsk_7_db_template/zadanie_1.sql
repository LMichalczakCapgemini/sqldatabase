-- -- -------------------------------------------------------------------------------
-- -- Zadanie 1
-- -- -------------------------------------------------------------------------------

-- -- -------------------------------------------------------------------------------
-- Section: setting sql_mode
-- -- -------------------------------------------------------------------------------
-- Key settings:
-- ONLY_FULL_GROUP_BY = gwarantuje swiadome deklarowanie sekcji GROUP BY
-- STRICT_ALL_TABLES = gwarantuje, że wstawicie odpowiednie typy/długości danych w odpowiednie miejsca
--		DB automatycznie nie będzie konwertował typów ani nie ucinał długości znaków.
--		Czyli nie bedzie mozliwe wstawienie stringa '10/24' do pola INT(10) ani stringa
--		o dlugosci 100 znakow w pole VARCHAR(10). Przy wlaczonym STRICT_ALL_TABLES pojawi sie Error
--		a nie Warning.
-- SQL Modes: https://dev.mysql.com/doc/refman/8.0/en/sql-mode.html
-- -- -------------------------------------------------------------------------------

SET sql_mode='ONLY_FULL_GROUP_BY,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -- -------------------------------------------------------------------------------
-- Section: DROP DATABASE lub DROP Tables
-- -- -------------------------------------------------------------------------------
-- Tutaj usuwamy całą bazę przed jej zadeklarowaniem. To pozwoli Wam łatwo resetować
-- bazę danych w trakcie testów.
-- -- -------------------------------------------------------------------------------

DROP DATABASE IF EXISTS jsk_v7_lukasz_michalczak;

-- -- -------------------------------------------------------------------------------
-- Section: USE
-- Zamien 'jsk_v7_jan_kowalski' na poprawna nazwe Twojej bazy danych
-- Format nazwy DB: jsk_v7_[imie]_[nazwisko]
-- -- -------------------------------------------------------------------------------
CREATE DATABASE jsk_v7_lukasz_michalczak;
USE jsk_v7_lukasz_michalczak;

-- -- -------------------------------------------------------------------------------
-- Section: CREATE
-- -- -------------------------------------------------------------------------------
-- Tutaj tworzymy nasze tabele bez kluczy obcych. Definiujemy tylko i wyłącznie tabele.
-- W sekcji niżej należy pododawać relacje między tabelami. To daje większą przejrzystość.
-- -- -------------------------------------------------------------------------------

CREATE TABLE Placówki (
    Id INT NOT NULL AUTO_INCREMENT,
    FK_DaneKontaktowe_Id INT NOT NULL,
    PRIMARY KEY (Id)
);

CREATE TABLE Pracownicy (
    Id INT NOT NULL AUTO_INCREMENT,
    Stanowisko ENUM('SPRZEDAWCA', 'KIEROWNIK', 'KSIĘGOWY'),
    FK_Placówka_Id INT NOT NULL,
    FK_DaneOsobowe_Id INT NOT NULL,
    PRIMARY KEY (Id)
);

CREATE TABLE Klienci (
    Id INT NOT NULL AUTO_INCREMENT,
    KartaKredytowa DECIMAL(16) NOT NULL,
    FK_DaneKontaktowe_Id INT NOT NULL,
    FK_DaneOsobowe_Id INT NOT NULL,
    PRIMARY KEY (Id)
);

CREATE TABLE Samochody (
    Id INT NOT NULL AUTO_INCREMENT,
    TypSamochodu VARCHAR(15) NULL,
    Marka VARCHAR(15) NOT NULL,
    RokProdukcji DATE NULL,
    Kolor VARCHAR(15) NULL,
    PojemnośćSilnika VARCHAR(15) NULL,
    MocSilnika_KM DECIMAL(4) NULL,
    Przebieg_km DECIMAL(7) NULL,
    PRIMARY KEY (Id)
);

CREATE TABLE Wypożyczenia (
    Id INT NOT NULL AUTO_INCREMENT,
    DataWypożyczenia DATE NOT NULL,
    DataOddania DATE NOT NULL,
    Koszt DOUBLE(7 , 2 ) NOT NULL,
    FK_PlacówkaOddania_Id INT NOT NULL,
    FK_PlacówkaWypożyczenia_Id INT NOT NULL,
    FK_Samochód_Id INT NOT NULL,
    FK_Klient_Id INT NOT NULL,
    PRIMARY KEY (Id)
);

CREATE TABLE DaneKontaktowe(
	Id INT NOT NULL AUTO_INCREMENT,
    Kraj VARCHAR(60) NOT NULL,
    Miasto VARCHAR(60) NOT NULL,
    Ulica VARCHAR(100) NULL,
    NumerBudynku VARCHAR(5) NOT NULL,
    Telefon VARCHAR(15) NULL,
    Email VARCHAR(40) NULL,
	PRIMARY KEY (Id)
);

CREATE TABLE Daneosobowe(
	Id INT NOT NULL AUTO_INCREMENT,
    Imię VARCHAR(20) NOT NULL,
    Nazwisko VARCHAR(20) NOT NULL,
    DataUrodzenia DATE NULL,
	PRIMARY KEY (Id)
);

CREATE TABLE Pracownik2Samochód (
    Pracownik_Id INT NOT NULL,
    Samochód_Id INT NOT NULL,
    PRIMARY KEY (Pracownik_Id , Samochód_Id)
);


-- -- -------------------------------------------------------------------------------
-- Section: ALTER TABLE
-- -- -------------------------------------------------------------------------------
-- Tutaj dodajemy klucze obce oraz ewentualne zmiany w tabelach.
-- -- -------------------------------------------------------------------------------

ALTER TABLE Pracownicy
ADD CONSTRAINT fk_Pracownicy_Placówki
FOREIGN KEY (FK_Placówka_Id) REFERENCES Placówki(Id);


ALTER TABLE Wypożyczenia
ADD CONSTRAINT fk_Wypożyczenia_Klienci
FOREIGN KEY (FK_Klient_Id) REFERENCES Klienci(Id),

ADD CONSTRAINT fk_Wypożyczenia_Samochody
FOREIGN KEY (FK_Samochód_Id) REFERENCES Samochody(Id),

ADD CONSTRAINT fk_Wypożyczenia_PlacówkaWypożyczenia
FOREIGN KEY (FK_PlacówkaWypożyczenia_Id) REFERENCES Placówki(Id),

ADD CONSTRAINT fk_Wypożyczenie_PlacówkaOddania
FOREIGN KEY (FK_PlacówkaOddania_Id) REFERENCES Placówki(Id);


ALTER TABLE Placówki
ADD CONSTRAINT fk_Placówki_DaneKontaktowe
FOREIGN KEY (FK_DaneKontaktowe_Id) REFERENCES DaneKontaktowe(Id);

ALTER TABLE Pracownicy
ADD CONSTRAINT fk_Pracownicy_DaneOsobowe
FOREIGN KEY (FK_DaneOsobowe_Id) REFERENCES DaneOsobowe(Id);

ALTER TABLE Klienci
ADD CONSTRAINT fk_Klienci_DaneKontaktowe
FOREIGN KEY (FK_DaneKontaktowe_Id) REFERENCES DaneKontaktowe(Id),

ADD CONSTRAINT fk_Klienci_DaneOsobowe
FOREIGN KEY (FK_DaneOsobowe_Id) REFERENCES DaneOsobowe(Id);